import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

public class ParseData
{
    private static final ArrayList<String> strings = new ArrayList<>();
    public static final String INPUT1 = "data/Continents.txt";
    public static final String INPUT2 = "data/Countries.txt";
    public static final String INPUT3 = "data/Waters.txt";

    public static final List<String> list1;
    public static final List<String> list2;
    public static final List<String> list3;
    public static final HashMap<String, DataContinents> map = new LinkedHashMap<>();
    public static final HashMap<String, DataCountries> map2 = new LinkedHashMap<>();
    public static final HashMap<DataCountries, DataContinents> map3 = new LinkedHashMap<>();
    public static final HashMap<DataWaters, DataCountries> map4 = new LinkedHashMap<>();
    public static final HashMap<List<DataWaters>, DataCountries> map5 = new LinkedHashMap<>();
    public static final HashSet<DataContinents> dataContinents = new HashSet<>();

    public static final List<String> onlyWaters = new ArrayList<>();

    static
    {
        try
        {
            list1 = Files.readAllLines(Path.of(INPUT1));
            list2 = Files.readAllLines(Path.of(INPUT2));
            list3 = Files.readAllLines(Path.of(INPUT3));
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    public static void searchContinents()
    {
        WebClient webClient = new WebClient(BrowserVersion.CHROME);

        try
        {
            HtmlPage page = webClient.getPage("https://mehriban-aliyeva.az/ru/kids_oxu/846130");

            webClient.getCurrentWindow().getJobManager().removeAllJobs();
            webClient.getOptions().setCssEnabled(false);
            webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
            webClient.getOptions().setThrowExceptionOnScriptError(false);
            webClient.getOptions().setPrintContentOnFailingStatusCode(false);

            List<HtmlElement> items = page.getByXPath("//strong//em");
            if (!items.isEmpty())
            {
                for (HtmlElement item : items)
                {
                    String a = item.getTextContent();
                    strings.add(a);
                }
            }
        }
        catch (IOException e)
        {
            System.out.println("An error occurred: " + e);
        }
    }
    public void createFile() throws Exception
    {
        searchContinents();
        FileWriter fw = new FileWriter(INPUT1);
        String continents = strings.stream().collect(Collectors.joining("\n", "", ""));
        String nextText = continents.replaceAll(", ", "\n");
        String nextText2 = nextText.replaceAll(" и ", "\n");
        String nextText3 = nextText2.replaceAll("\\.", "");
        fw.write(nextText3);
        fw.flush();
        fw.close();
    }
    public void parseContinents()
    {
        DataContinents dataContinent;
        for (String line : list1)
        {
            dataContinent = new DataContinents(line);
            dataContinents.add(dataContinent);
        }
    }

    public void parseCountry()
    {
        DataContinents dataContinents1;
        DataCountries dataCountries;
        for (int i = 0, j = 1; i < list2.size() && j < list2.size(); i = i + 3, j = j + 3)
        {
            dataContinents1 = new DataContinents(list2.get(j));
            map.put(list2.get(i), dataContinents1);
            dataCountries = new DataCountries(list2.get(i), dataContinents1);
            map3.put(dataCountries, dataContinents1);
        }
    }
    public List<DataWaters> parseWaters()
    {
        List<DataWaters> dataWaters = new ArrayList<>();
        DataCountries dataCountry1;
        DataWaters dataWaters1;
        for
        (
                int i = 0, j = 1, a = 2, d = 3, r = 4, z = 5, o = 6;
                i < list3.size() && j < list3.size() && a < list3.size() && d < list3.size() && r < list3.size() && z < list3.size() && o < list3.size();
                i = i + 8, j = j + 8, a = a + 8, d = d + 8, r = r + 8, z = z + 8, o = o + 8
        )
        {
            dataCountry1 = new DataCountries(list3.get(z), map.get(list3.get(z)));
            dataWaters1 = new DataWaters
                    (
                            list3.get(i), list3.get(j), Integer.parseInt(list3.get(a)), Integer.parseInt(list3.get(d)),
                            list3.get(r), dataCountry1, list3.get(o)
                    );
            dataWaters.add(dataWaters1);
            map2.put(list3.get(i), dataCountry1);
            map4.put(dataWaters1, dataCountry1);
            map5.put(dataWaters, dataCountry1);
            onlyWaters.add(list3.get(i));
        }
        return dataWaters;
    }
}